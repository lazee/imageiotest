package net.jakobnielsen;

import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentSampleModel;
import java.awt.image.DataBuffer;
import java.awt.image.IndexColorModel;
import java.awt.image.MultiPixelPackedSampleModel;
import java.awt.image.PackedColorModel;
import java.awt.image.SampleModel;
import java.awt.image.SinglePixelPackedSampleModel;
import java.awt.image.WritableRaster;

public class ImageDump {

    public static String dump(BufferedImage image) {
        StringBuffer sb = new StringBuffer();
        println(sb, dumpAttributes(image));
        println(sb, dumpColorModel(image.getColorModel()));
        println(sb, dumpRaster(image.getRaster()));
        return sb.toString();
    }

    public static String dumpAttributes(BufferedImage image) {
        StringBuffer sb = new StringBuffer();
        println(sb, "BASIC BUFFEREDIMAGE ATTRIBUTES:");
        println(sb, "    instance of " + image.getClass().getName());
        println(sb, "    height=" + image.getHeight());
        println(sb, "    width=" + image.getWidth());
        println(sb, "    minX=" + image.getMinX());
        println(sb, "    minY=" + image.getMinY());
        print(sb, "  type=" + image.getType() + " (");
        print(sb, getImageTypeString(image.getType()));
        println(sb, ")");
        println(sb, "    isAlphaPremultiplied=" + image.isAlphaPremultiplied());
        return sb.toString();
    }

    public static String getImageTypeString(int type) {
        switch (type) {
            case BufferedImage.TYPE_CUSTOM:
                return "TYPE_CUSTOM";
            case BufferedImage.TYPE_INT_RGB:
                return "TYPE_INT_RGB";
            case BufferedImage.TYPE_INT_ARGB:
                return "TYPE_INT_ARGB";
            case BufferedImage.TYPE_INT_ARGB_PRE:
                return "TYPE_INT_ARGB_PRE";
            case BufferedImage.TYPE_INT_BGR:
                return "TYPE_INT_BGR";
            case BufferedImage.TYPE_3BYTE_BGR:
                return "TYPE_3BYTE_BGR";
            case BufferedImage.TYPE_4BYTE_ABGR:
                return "TYPE_4BYTE_ABGR";
            case BufferedImage.TYPE_4BYTE_ABGR_PRE:
                return "TYPE_4BYTE_ABGR_PRE";
            case BufferedImage.TYPE_USHORT_565_RGB:
                return "TYPE_USHORT_565_RGB";
            case BufferedImage.TYPE_USHORT_555_RGB:
                return "TYPE_USHORT_555_RGB";
            case BufferedImage.TYPE_BYTE_GRAY:
                return "TYPE_BYTE_GRAY";
            case BufferedImage.TYPE_USHORT_GRAY:
                return "TYPE_USHORT_GRAY";
            case BufferedImage.TYPE_BYTE_BINARY:
                return "TYPE_BYTE_BINARY";
            case BufferedImage.TYPE_BYTE_INDEXED:
                return "TYPE_BYTE_INDEXED";
            default:
                return "unknown type?";
        }
    }

    public static String dumpColorModel(ColorModel colorModel) {
        StringBuffer sb = new StringBuffer();
        println(sb, dumpColorSpace(colorModel.getColorSpace()));
        println(sb, "COLOR MODEL:");
        println(sb, "    instance of " + colorModel.getClass().getName());
        println(sb, "    hasAlpha=" + colorModel.hasAlpha());
        println(sb, "    isAlphaPremultiplied=" + colorModel.isAlphaPremultiplied());
        print(sb, "  transparency=" + colorModel.getTransparency() + " (");
        print(sb, getTransparencyString(colorModel.getTransparency()));
        println(sb, ")");
        print(sb, "  transferType=" + colorModel.getTransferType() + " (");
        print(sb, getTypeString(colorModel.getTransferType()));
        println(sb, ")");
        println(sb, "    numComponents=" + colorModel.getNumComponents());
        println(sb, "    numColorComponents=" + colorModel.getNumColorComponents());
        println(sb, "    pixelSize(bits/pixel)=" + colorModel.getPixelSize());
        for (int i = 0, ub = colorModel.getNumComponents(); i < ub; ++i) {
            println(sb, "    componentSize[" + i + "]=" + colorModel.getComponentSize(i));
        }

        if (colorModel instanceof IndexColorModel) {
            println(sb, dumpIndexColorModel((IndexColorModel) colorModel));
        } else if (colorModel instanceof PackedColorModel) {
            println(sb, dumpPackedColorModel((PackedColorModel) colorModel));
        }
        return sb.toString();
    }

    public static String dumpIndexColorModel(IndexColorModel colorModel) {
        StringBuffer sb = new StringBuffer();
        println(sb, "    mapSize=" + colorModel.getMapSize());
        println(sb, "    isValid=" + colorModel.isValid());
        println(sb, "    transparentPixel=" + colorModel.getTransparentPixel());
        return sb.toString();
    }

    public static String dumpPackedColorModel(PackedColorModel colorModel) {
        StringBuffer sb = new StringBuffer();
        int[] masks = colorModel.getMasks();
        for (int i = 0; i < masks.length; ++i) {
            println(sb, "    masks[" + i + "]=" + Integer.toHexString(masks[i]));
        }
        return sb.toString();
    }

    public static String getTransparencyString(int transparency) {
        switch (transparency) {
            case Transparency.OPAQUE:
                return "OPAQUE";
            case Transparency.BITMASK:
                return "BITMASK";
            case Transparency.TRANSLUCENT:
                return "TRANSLUCENT";
            default:
                return "unknown transparency?";
        }
    }

    public static String getTypeString(int type) {
        switch (type) {
            case DataBuffer.TYPE_BYTE:
                return "BYTE";
            case DataBuffer.TYPE_USHORT:
                return "USHORT";
            case DataBuffer.TYPE_SHORT:
                return "SHORT";
            case DataBuffer.TYPE_INT:
                return "INT";
            case DataBuffer.TYPE_FLOAT:
                return "FLOAT";
            case DataBuffer.TYPE_DOUBLE:
                return "DOUBLE";
            default:
                return "unknown type?";
        }
    }

    public static String dumpColorSpace(ColorSpace colorSpace) {
        StringBuffer sb = new StringBuffer();
        println(sb, "COLOR SPACE:");
        println(sb, "    instance of " + colorSpace.getClass().getName());
        println(sb, "    isCS_sRGB=" + colorSpace.isCS_sRGB());
        print(sb, "  type=" + colorSpace.getType() + " (");
        print(sb, getColorSpaceTypeString(colorSpace.getType()));
        println(sb, ")");
        println(sb, "    numComponents=" + colorSpace.getNumComponents());
        for (int i = 0, ub = colorSpace.getNumComponents(); i < ub; ++i) {
            print(sb, "  name[" + i + "]=" + colorSpace.getName(i));
            print(sb, ", minValue=" + colorSpace.getMinValue(i));
            println(sb, ", maxValue=" + colorSpace.getMaxValue(i));
        }
        return sb.toString();
    }

    public static String getColorSpaceTypeString(int type) {
        switch (type) {
            case ColorSpace.TYPE_XYZ:
                return "TYPE_XYZ";
            case ColorSpace.TYPE_Lab:
                return "TYPE_Lab";
            case ColorSpace.TYPE_Luv:
                return "TYPE_Luv";
            case ColorSpace.TYPE_YCbCr:
                return "TYPE_YCbCr";
            case ColorSpace.TYPE_Yxy:
                return "TYPE_Yxy";
            case ColorSpace.TYPE_RGB:
                return "TYPE_RGB";
            case ColorSpace.TYPE_GRAY:
                return "TYPE_GRAY";
            case ColorSpace.TYPE_HSV:
                return "TYPE_HSV";
            case ColorSpace.TYPE_HLS:
                return "TYPE_HLS";
            case ColorSpace.TYPE_CMYK:
                return "TYPE_CMYK";
            case ColorSpace.TYPE_CMY:
                return "TYPE_CMY";
            case ColorSpace.TYPE_2CLR:
                return "TYPE_2CLR";
            case ColorSpace.TYPE_3CLR:
                return "TYPE_3CLR";
            case ColorSpace.TYPE_4CLR:
                return "TYPE_4CLR";
            case ColorSpace.TYPE_5CLR:
                return "TYPE_5CLR";
            case ColorSpace.TYPE_6CLR:
                return "TYPE_6CLR";
            case ColorSpace.TYPE_7CLR:
                return "TYPE_7CLR";
            case ColorSpace.TYPE_8CLR:
                return "TYPE_8CLR";
            case ColorSpace.TYPE_9CLR:
                return "TYPE_9CLR";
            case ColorSpace.TYPE_ACLR:
                return "TYPE_ACLR";
            case ColorSpace.TYPE_BCLR:
                return "TYPE_BCLR";
            case ColorSpace.TYPE_CCLR:
                return "TYPE_CCLR";
            case ColorSpace.TYPE_DCLR:
                return "TYPE_DCLR";
            case ColorSpace.TYPE_ECLR:
                return "TYPE_ECLR";
            case ColorSpace.TYPE_FCLR:
                return "TYPE_FCLR";
            default:
                return "unknown type?";
        }
    }

    public static String dumpRaster(WritableRaster raster) {
        StringBuffer sb = new StringBuffer();
        println(sb, "RASTER:");
        println(sb, "    instance of " + raster.getClass().getName());
        println(sb, "    height=" + raster.getHeight());
        println(sb, "    width=" + raster.getWidth());
        println(sb, "    minX=" + raster.getMinX());
        println(sb, "    minY=" + raster.getMinY());
        println(sb, "    sampleModelTranslateX=" + raster.getSampleModelTranslateX());
        println(sb, "    sampleModelTranslateY=" + raster.getSampleModelTranslateY());
        println(sb, "    numBands=" + raster.getNumBands());
        println(sb, "    numDataElements=" + raster.getNumDataElements());
        print(sb, "  transferType=" + raster.getTransferType() + " (");
        print(sb, getTypeString(raster.getTransferType()));
        println(sb, ")");
        println(sb, "    parent is null=" + (null == raster.getParent()));
        println(sb, dumpDataBuffer(raster.getDataBuffer()));
        println(sb, dumpSampleModel(raster.getSampleModel()));
        return sb.toString();
    }

    public static String dumpDataBuffer(DataBuffer dataBuffer) {
        StringBuffer sb = new StringBuffer();
        println(sb, "DATA BUFFER:");
        println(sb, "    instance of " + dataBuffer.getClass().getName());
        print(sb, "  dataType=" + dataBuffer.getDataType() + " (");
        print(sb, getTypeString(dataBuffer.getDataType()));
        println(sb, ")");
        println(sb, "    numBanks=" + dataBuffer.getNumBanks());
        println(sb, "    size=" + dataBuffer.getSize());
        for (int i = 0, ub = dataBuffer.getNumBanks(); i < ub; ++i) {
            println(sb, "    offset[" + i + "]=" + dataBuffer.getOffsets()[i]);

        }
        return sb.toString();
    }

    public static String dumpSampleModel(SampleModel sampleModel) {
        StringBuffer sb = new StringBuffer();
        println(sb, "SAMPLE MODEL:");
        println(sb, "    instance of " + sampleModel.getClass().getName());
        println(sb, "    height=" + sampleModel.getHeight());
        println(sb, "    width=" + sampleModel.getWidth());
        print(sb, "  transferType=" + sampleModel.getTransferType() + " (");
        print(sb, getTypeString(sampleModel.getTransferType()));
        println(sb, ")");
        print(sb, "  dataType=" + sampleModel.getDataType() + " (");
        print(sb, getTypeString(sampleModel.getDataType()));
        println(sb, ")");
        println(sb, "    numBands=" + sampleModel.getNumBands());
        println(sb, "    numDataElements=" + sampleModel.getNumDataElements());
        int[] sampleSize = sampleModel.getSampleSize();
        for (int i = 0, ub = sampleSize.length; i < ub; ++i) {
            println(sb, "    sampleSize[" + i + "]=" + sampleSize[i]);
        }
        if (sampleModel instanceof SinglePixelPackedSampleModel) {
            println(sb, dumpSinglePixelPackedSampleModel((SinglePixelPackedSampleModel) sampleModel));
        } else if (sampleModel instanceof MultiPixelPackedSampleModel) {
            println(sb, dumpMultiPixelPackedSampleModel((MultiPixelPackedSampleModel) sampleModel));
        } else if (sampleModel instanceof ComponentSampleModel) {
            println(sb, dumpComponentSampleModel((ComponentSampleModel) sampleModel));
        }
        return sb.toString();
    }

    public static String dumpSinglePixelPackedSampleModel(SinglePixelPackedSampleModel sampleModel) {
        StringBuffer sb = new StringBuffer();
        println(sb, "    scanlineStride=" + sampleModel.getScanlineStride());
        int[] bitMasks = sampleModel.getBitMasks();
        for (int i = 0; i < bitMasks.length; ++i) {
            println(sb, "    bitmasks[" + i + "]=" + Integer.toHexString(bitMasks[i]));
        }
        return sb.toString();
    }

    public static String dumpMultiPixelPackedSampleModel(MultiPixelPackedSampleModel sampleModel) {
        StringBuffer sb = new StringBuffer();
        println(sb, "    scanlineStride=" + sampleModel.getScanlineStride());
        println(sb, "    pixelBitStride=" + sampleModel.getPixelBitStride());
        return sb.toString();
    }

    public static String dumpComponentSampleModel(ComponentSampleModel sampleModel) {
        StringBuffer sb = new StringBuffer();
        println(sb, "    scanlineStride=" + sampleModel.getScanlineStride());
        println(sb, "    pixelStride=" + sampleModel.getPixelStride());
        int[] bandOffsets = sampleModel.getBandOffsets();
        for (int i = 0; i < bandOffsets.length; ++i) {
            println(sb, "    bandOffsets[" + i + "]=" + bandOffsets[i]);
        }
        int[] bankIndices = sampleModel.getBankIndices();
        for (int i = 0; i < bankIndices.length; ++i) {
            println(sb, "    bankIndices[" + i + "]=" + bankIndices[i]);
        }
        return sb.toString();
    }

    private static void println(StringBuffer sb, String s) {
        sb.append(s + "\n");
    }

    private static void print(StringBuffer sb, String s) {
        sb.append(s);
    }
}
