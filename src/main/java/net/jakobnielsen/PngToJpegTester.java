package net.jakobnielsen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DirectColorModel;
import java.io.File;
import java.io.IOException;

public class PngToJpegTester {

    private static final Logger log = LoggerFactory.getLogger(PngToJpegTester.class);


    /**
     * Solution taken from : https://community.oracle.com/thread/1264811?start=0&tstart=0
     */
    public void writeJpeg(BufferedImage image, File out) throws IOException {
        ImageWriter writer = ImageIO.getImageWritersByFormatName("jpeg").next();
        ImageWriteParam param = writer.getDefaultWriteParam();
        param.setSourceBands(new int[] {0, 1, 2});
        ColorModel cm = new DirectColorModel(24,
                                             0x00ff0000,     // Red
                                             0x0000ff00,     // Green
                                             0x000000ff,     // Blue
                                             0x0);           // Alpha
        param.setDestinationType(new ImageTypeSpecifier(cm, cm.createCompatibleSampleModel(1, 1)));
        ImageOutputStream outStream = ImageIO.createImageOutputStream(out);
        writer.setOutput(outStream);
        writer.write(null,new IIOImage(image,null,null),param);
        writer.dispose();
        outStream.close();
    }

    public void run(File from, File to) {


        try {
            // Standard ImageIO
            BufferedImage img = ImageIO.read(from);
            // The new way
            writeJpeg(img, to);
            // The old way that gives an error
            // ImageIO.write(img, "jpeg", to);
        } catch (IOException e) {
            log.error("Ran into a problem:" + e.getMessage());
        }

    }

    public static void main(String[] args) {
        PngToJpegTester p = new PngToJpegTester();
        if (args.length != 2) {
            log.error("You must give the png as the first parameter and the output jpg file name as the second");
        } else {
            p.run(new File(args[0]), new File(args[1]));
        }
    }
}
